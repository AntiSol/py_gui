"""
AntiSol's GUI classes
(C)opyright Dale Magee, 2012 - 2020
GPLv2 License.

This is a collection of helper classes for easily creating GTK interfaces

While this file was written for RETROGAMER, it's intended to be general
	enough that this file can be used in other python projects - nothing
	retrogamer-specific should be included in this file.


"""
import gtk,atk
import math,time, sys, os, glob


class Icon(gtk.Image):
	"""
	A gtk.Image descendant which auto-loads a file and resizes it to
		a specific (square) size
	"""
	def __init__(self,filename,size=32):
		gtk.Image.__init__(self)
		self._filename = filename
		pb = gtk.gdk.pixbuf_new_from_file(filename)
		pb = pb.scale_simple(size, size, gtk.gdk.INTERP_BILINEAR)
		self.set_from_pixbuf(pb)

class BetterDialog(gtk.Dialog):
	"""
	An improved dialog with a bunch of helper functions
	"""
	def __init__(self, title=None, parent=None, flags=0, buttons=None):
		gtk.Dialog.__init__(self, title, parent, flags, buttons)
		
		#'OK' is the default response:
		self.set_default_response(gtk.RESPONSE_ACCEPT)
		
		#add some padding around the content:
		#container = self.get_content_area()
		#self.set_border_width(5)
		
		#self.connect('delete_event', self.terminate_program)

	#def terminate_program(self,*args):
	#	gtk.main_quit()
	#	sys.exit(0)

	def find_instances(self, widgets,classname):
		"""
		given a bunch of widgets (return from gtk.container.get_children())
			recursively walks through hierarchy
			and returns all instances of the provided class as an array
		"""
		ret = []
		for widget in widgets:
			if isinstance(widget, classname):
				ret.append(widget)
			elif isinstance(widget, gtk.Container):
				ret = ret + self.find_instances(widget.get_children(),classname)

		return ret

	def add_row(self, widget,container = None):
		"""
		Adds a GTK widget to the dialog.
		"""
		if container != None:
			box = container
		else:
			box = self.get_content_area()
		hbox = gtk.HBox()
		hbox.pack_start(widget, True, True, 5)
		box.pack_start(hbox, False, True, 5)

	def add_label(self, text, align=0,padding = 5,container = None):
		"""
		Adds a label to the dialog (as a row)
		"""
		if container != None:
			box = container
		else:
			box = self.get_content_area()
		label = gtk.Label()
		label.set_markup(text)
		label.set_alignment(align, 0.5)
		#add x padding:
		label.set_padding(5,0)
		box.pack_start(label, False,False,padding)
		return label


class YesNoDialog(BetterDialog):
	"""
	A simple dialog with ok/cancel buttons and text.
		It's better to use YesNoDialog.show, which destroys the dialog
		automatically and returns a boolean (true == OK)
	"""
	@staticmethod
	def show(parent = None,text = None,title = None):
		#show a dialog, destroy it, and return a boolean
		dialog = YesNoDialog(parent,text,title)
		ret = (dialog.resp == gtk.RESPONSE_ACCEPT)
		dialog.destroy()
		return ret

	def __init__(self, parent = None,text = "Are You Sure?",title = "Question"):
		"""
		parent is a GtkAppList
		"""
		gtk.Dialog.__init__(self, title, parent,
			gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
			(gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
			gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))

		self.add_label("%s" % text,0.5,15)

		self.show_all()
		self.resp = self.run()


class OKDialog(BetterDialog):
	"""
	A simple dialog with ok button and text.
		It's better to use OKDialog.show, which destroys the dialog
		automatically and returns a boolean (true == OK)
	"""
	@staticmethod
	def show(parent = None,text = None,title = None):
		#show a dialog, destroy it, and return a boolean
		dialog = OKDialog(parent,text,title)
		ret = (dialog.resp == gtk.RESPONSE_ACCEPT)
		dialog.destroy()
		return ret

	def __init__(self, parent = None,text = "Are You Sure?",title = "Question",run=True):
		"""
		parent is a GtkAppList
		"""
		#print "Parent: '%s'" % parent
		gtk.Dialog.__init__(self, title, parent,
			gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
			(gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))

		self.add_label("%s" % text,0.5,15)

		self.show_all()
		if run:
			self.resp = self.run()
		else:
			self.resp = None


class TextInputDialog(BetterDialog):
	"""
	A simple dialog with OK/Cancel buttons and a text input
	it's best to use GetTextInputDialog.show
	"""
	
	@staticmethod
	def show(parent = None,text = None, default = "", title = None):
		"""
		Show a dialog and return a tuple containing a boolean indicating
			the user's action (true==ok, false==cancel) and the contents
			of the textbox
		"""
		dlg = TextInputDialog(parent,text,default,title)
		ret=(dlg.resp == gtk.RESPONSE_ACCEPT)
		txt=dlg.textbox.get_text()
		dlg.destroy()
		return (ret,txt)
	
	def __init__(self,parent = None,text = None,default = "",title = None):
		gtk.Dialog.__init__(self, title, parent,
			gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
			(gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
			gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))

		self.add_label("%s" % text,0,15)
		
		self.textbox = gtk.Entry()
		self.textbox.set_text(default)
		self.add_row(self.textbox)
		self.textbox.connect('activate',self.enter_press)

		self.show_all()
		self.resp = self.run()
					
	def enter_press(self,widget = None):
		"""
		pressing enter in the textbox is equivalent to pressing OK
		"""
		self.response(gtk.RESPONSE_ACCEPT)
		


class ProgressDialog(gtk.Window):
	"""
	A GTK dialog containing a progressbar which doesn't block your code by
		calling gtk_main().

	To use it:
	dlg = ProgressDialog("Window Title","Doing Something")
	for x in range(0..100):
		#Do something
		dlg.update(float(x)/100)
	dlg.destroy()

	"""
	def __init__(self, title="Working...", text=""):
		"""
		Create the progress window
		"""
		# update immediately: set last update to 60s ago
		self.last_update = time.time() - 60
		gtk.Window.__init__(self, gtk.WINDOW_TOPLEVEL)

		self.set_resizable(True)
		self.set_default_size(480, 48)
		self.set_deletable(False)
		self.set_type_hint(gtk.gdk.WINDOW_TYPE_HINT_DIALOG)
		self.set_position(gtk.WIN_POS_CENTER_ALWAYS)

		# self.window.connect("destroy", self.destroy_progress)
		self.set_title(title)
		self.set_border_width(0)

		vbox = gtk.VBox(False, 5)
		vbox.set_border_width(10)
		self.add(vbox)

		# Create the ProgressBar
		self.progressbar = gtk.ProgressBar()
		self.progressbar.padding = 10
		self.progressbar.set_size_request(480, 32)
		self.progressbar.set_text(text)

		vbox.add(self.progressbar)

		self.show_all()
		self.gtk_update()

	def update(self, position=None, text=None, update=True):
		"""
		Updates the dialog.

		position is a float between 0 and 1
		if text is provided the window text will be updated
		if update is False, properties will be updated but gtk_update
			will not be called
		"""
		if position != None:
			if position < 0: position = 0
			if position > 1: position = 1
			self.progressbar.set_fraction(position)

		if text != None:
			self.progressbar.set_text(text)

		if update:
			self.gtk_update()

	def pulse(self, size=0.05):
		"""
		Enable pulse mode. You'll still need to call update() or gtk_update
			regularly.
		"""
		self.progressbar.set_pulse_step(size)
		self.progressbar.pulse()
		self.gtk_update()

	def gtk_update(self, force=True):
		"""
		This is how we get around calling gtk_main or running in another
			thread. This needs to happen regularly
		"""

		# limit frame rate
		if not force and (time.time() - self.last_update < 0.2):
			return False

		gtk.main_iteration(False)
		while gtk.events_pending():
				gtk.main_iteration(False)

		self.last_update = time.time()



class ColumnBox(gtk.VBox):
	"""
	This is an extended VBox with columns, adding left-to-right and then
	starting a new row.

	Just supply a column count when creating it, then call add_item()
		many times to add controls.

	It's important to call show() on the ColumnBox after you've finished
		adding controls, as this finalises the last row. Unfortunately calling
		show_all on a parent isn't good enough :(

	"""
	def __init__(self, columns=2):

		gtk.VBox.__init__(self)
		self.columns = columns
		self.row = 1
		self.column = 0
		self.hbox = gtk.HBox()

	def add_item(self, item,colspan = 1, expand = True,padding = 5):
		#NOTE: colspan doesn't work properly :(
		self.column = self.column + colspan
		self.hbox.pack_start(item,expand,True,padding)

		if self.column >= self.columns:
			if not self.hbox.get_parent():
				self.pack_start(self.hbox,False,True,5)
			self.hbox = gtk.HBox()
			self.row = self.row + 1
			self.column = 0

	def show(self):
		if self.column > 0:		#dont add empty hbox
			if not self.hbox.get_parent():
				self.pack_start(self.hbox,False,True,5)
		gtk.VBox.show(self)

	def show_all(self):
		#add final row:
		self.show()
		gtk.VBox.show_all(self)


class ArrayEditor(gtk.ScrolledWindow):
	"""
	A GTK List which edits an array of strings
	"""
	def __init__(self,title = ""):
		gtk.ScrolledWindow.__init__(self)
		self.set_size_request(400,0)
		self.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
		self.treeview = gtk.TreeView()
		column = gtk.TreeViewColumn(title)
		self.treeview.append_column(column)
		self.store = gtk.ListStore(str)
		self.treeview.set_model(self.store)

		cr = gtk.CellRendererText()
		cr.set_property('editable', True)
		cr.connect("edited", self.cr_text_edited)

		column.pack_start(cr, True)
		column.add_attribute(cr, "text", 0)
		self.on_change = None

		self.add(self.treeview)
		self.show_all()

	def cr_text_edited(self, cr, row, txt):
		itr = self.store.get_iter(row)
		if self.store.get_value(itr,0) == "":
			#new item
			self.add_item(txt)
		else:
			self.store.set_value(itr,0,txt)

		if self.on_change:
			self.on_change(self)


	def connect(self,signal,callback):
		if signal == "changed":
			self.on_change = callback
		else:
			gtk.ScrolledWindow.connect(self,signal,callback)

	def val(self,val = None):
		if val != None:
			self.store.clear()
			#empty item for editing:
			self.store.append([''])
			if isinstance(val,(list,tuple)):
				for itm in val:
					self.add_item(itm)
			else:
				self.add_item(val)


		ret = []
		itr = self.store.get_iter_first()

		while itr != None:
			itr2 = self.store.iter_next(itr)
			val = self.store.get_value(itr,0)

			if val:
				ret.append(val)
			itr = itr2
		return ret

	def add_item(self,text,itr = None):
		#we actually insert before the last item, the last item being
		# our empty 'add item' row.
		if itr == None:
			#suboptimal, but there doesn't seem to be a get_last:
			i = self.store.get_iter_first()
			while i != None:
				last = i
				i = self.store.iter_next(i)
			itr = last

		self.store.insert_before(itr,[text])


class MarkupButton(gtk.Button):
	"""
	A Button that can use pango markup in its label
	"""
	def __init__(self,markup=None, stock=None, use_underline=True):
		gtk.Button.__init__(self,markup,stock,use_underline)
		self.set_markup(markup)
		
	def set_markup(self,markup):
		for child in self.get_children():
			child.set_label(markup)
			child.set_use_markup(True)


class MarkupToggle(gtk.ToggleButton):
	"""
	A ToggleButton that can use pango markup in its label.
	"""
	def __init__(self,markup = None, use_underline=True):
		gtk.ToggleButton.__init__(self,markup,use_underline)
		self.set_markup(markup)
		
	def set_markup(self,markup):
		for child in self.get_children():
			child.set_label(markup)
			child.set_use_markup(True)


class LeftAlignLabel(gtk.Label):
	"""
	A gtk.Label which is left-aligned by default.
	"""
	def __init__(self,text = None):
		gtk.Label.__init__(self,text)
		self.set_alignment(0,0.5)


class FileChooserWidget(gtk.VBox):
	"""
	A widget which allows specifying a path or filename. This is a 
		textbox with a '...' button that pops up a file chooser dialog
		
	@see GtkFileChooserWidget, the old name for this class, kept for backwards
		compatibility.
		
	"""
	
	@staticmethod
	def run_dialog(mask = "*",default = None,title = None,typetext = None,save = False):
		"""
		A static helper to just show a file chooser dialog without needing a widget
			and return a filename, cleaning up the dialog properly.
		"""
		instance = FileChooserWidget(mask,default,title,typetext,save)
		instance.choose_file()
		val = instance.val()
		instance.destroy()
		return val

	def __init__(self,mask = "*",default = None,title = None,typetext = None,save = False):
		"""
		Create a file chooser widget. This is a textbox and a '...' button
			mask specifies what type of files are allowed:
				- use a glob string or an array of them to specify filetypes
				- as a special case, specify 'dir' to make this a directory
					chooser. return will include a trailing '/' in this case
			*title is the title of the chooser dialog
			*typetext is the text for 'files of type' (e.g: "icons")
			save specifies that this is a 'save file' dialog, as opposed to
				'open'. it makes no sense in 'dir' mode
			* sensible defaults are provided if not supplied
		"""
		gtk.VBox.__init__(self)
		if default == None: default = os.path.expanduser("~/")
		
		if not isinstance(mask,list) and mask != "dir":
			#convert one mask to an array with 1 item so it's either 'dir'
			# or an array:
			mask = [mask]
		self.mask = mask
		self.title = title
		self.typetext = typetext
		self.default = default
		self.save = save
		self.textbox = gtk.Entry()

		self.button = gtk.Button()
		self.button.set_label("...")
		self.button.connect("clicked",self.choose_file)
		
		self.hbox = gtk.HBox()
		
		self.textbox.connect('focus-out-event',self.textbox_changed)

		self.hbox.pack_start(self.textbox,True,True,5)
		self.hbox.pack_start(self.button,False,True,5)
		self.pack_start(self.hbox,False,False,0)
		
		self.val(default)
		
	def textbox_changed(self,widget = None,something = None):
		#if this is a directory chooser or an 'open' file chooser,
		# make sure that self.val() exists.
		if self.mask == "dir" or not self.save:
			#path must exist
			if not os.path.exists(self.val()):
				OKDialog.show("Path '%s' does not exist!" % self.val(),"Error")
				self.choose_file()
			

	def choose_file(self,widget = None):
		icon = gtk.STOCK_OPEN
		if self.mask == "dir":
			dirmode = True
			act = gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER
			if self.title == None:
				self.title = "Choose A Directory"
			if self.typetext == None:
				self.typetext = "Directories"
		else:
			dirmode = False
			if self.save:
				act = gtk.FILE_CHOOSER_ACTION_SAVE
				icon = gtk.STOCK_SAVE
			else:
				act = gtk.FILE_CHOOSER_ACTION_OPEN

			if self.title == None:
				self.title = "Choose A File"
			if self.typetext == None:
				self.typetext = "Files"


		chooser = gtk.FileChooserDialog(self.title, action = act,
					buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, icon, gtk.RESPONSE_OK))

		if not dirmode:
			f = gtk.FileFilter()

			f.set_name(self.typetext)
			for pat in self.mask:
				f.add_pattern(pat)

			chooser.set_filter(f)
		
		val = self.val()
		if val and os.path.exists(val):
			chooser.set_filename(val)
			chooser.set_current_folder_uri(os.path.dirname(val))
			chooser.set_current_name(os.path.basename(val))
		else:
			chooser.set_filename(self.default)
			chooser.set_current_folder_uri(os.path.dirname(self.default))
			chooser.set_current_name(os.path.basename(self.default))

		"""
		chooser.connect('selection-changed', self.preview_icon)

		preview = gtk.Image()
		preview.set_size_request(64,64)

		chooser.set_preview_widget(preview)
		chooser.set_preview_widget_active(True)
		"""

		response = chooser.run()
		if response == gtk.RESPONSE_OK:
			val = chooser.get_filename()
			if dirmode:
				val = val + "/"
			self.textbox.set_text(val)
			#self.app.icon(val)
			#self.icon_filename.set_markup("<small>%s</small>" % val)
			#pb = gtk.gdk.pixbuf_new_from_file(val)
			#pb = pb.scale_simple(32, 32, gtk.gdk.INTERP_BILINEAR)
			#self.icon.set_from_pixbuf(pb)
		chooser.destroy()

	def connect(self,signal,callback):
		if signal == "changed":
			self.textbox.connect(signal,callback)
		else:
			self.connect(signal,callback)

	def val(self,val = None):
		if val != None:
			self.textbox.set_text(val)
		
		return os.path.expanduser(self.textbox.get_text())

class FileGlobberWidget(FileChooserWidget):
	"""
	An enhanced file chooser which chooses a path, has a glob pattern chooser,
		and tells you how many files match your selection
		
	Note that for compatibility reasons .val() must return the contents of
		the file chooser, which is our directory chooser. To get the list of
		files matching the selected glob, use .getfiles()
		
	"""
	def __init__(self,mask = "*",default = None,title = None,typetext = None):
		
		#hack: we need to call hbox init so that we can pack our "path" label:
		gtk.VBox.__init__(self)
		lbl = gtk.Label("Path:")
		lbl.set_alignment(0,0.5)
		self.pack_start(lbl,False,False,5)
		
		#now init the file chooser:
		FileChooserWidget.__init__(self,"dir",os.path.expanduser(default),title,typetext,False)
		
		box = gtk.HBox()
		
		self.presetbox = gtk.HBox()
		
		self.case_sensitive = gtk.CheckButton("Case Sensitive")
		self.case_sensitive.connect('toggled',self.getcount)
		
		self.presetbox.pack_end(self.case_sensitive,False,False,5)
		
		#self.count = gtk.Label("0 files")
		
		self.count = gtk.LinkButton("","0 files")
		self.count.set_sensitive(False)
		self.count.connect('clicked',self.show_files)
		
		#now augment it with globber stuff:
		lbl = gtk.Label("Glob:")
		box.pack_start(lbl,False,False,5)
		
		self.globbox = gtk.Entry()
		#mask = '*.[Jj][Pp][Gg],*.[Pp][Nn][Gg]'
		self.globbox.set_text(mask)
		
		self.globbox.connect('focus-out-event',self.globbox_changed)
		self.globbox.connect('changed',self.globbox_changed)
		self.textbox.connect('changed',self.getcount)
		
		box.pack_start(self.globbox,False,False,5) #True,True,5)
		
		box.pack_start(self.count,False,False,5)
		
		self.pack_start(box,True,True,5)
		#self.pack_start(self.case_sensitive,False,False,5)
		
		self.pack_start(self.presetbox,False,False,5)
		
		self.add_preset("*")
		
		self.show_all()
		self.getcount()
		
	def show_files(self,widget=None):
		"""
		Show a dialog with a list of currently selected files
		"""
		files = self.getfiles()
		txt = ""
		for f in files:
			if txt: txt += "\n"
			txt += os.path.basename(f)
		txt+="\n"
		
		dlg = OKDialog(None,"Matching Files for glob '%s':" % self.getglob(),"Matching Files",False)
		
		textbox = MultiLineTextBox()
		textbox.set_size_request(200,400)
		textbox.text(txt)
		textbox.readonly(True)
		dlg.get_content_area().pack_start(textbox,True,True,5)
		dlg.show_all()
		
		dlg.run()
		
		dlg.destroy()
		
		
		
	def preset_press(self,widget = None,mask=None):
		#print("Preset press. Mask: %s" % mask)
		self.globbox.set_text(mask)
		self.globbox_changed()
		
	def add_preset(self,mask,label=None):
		"""
		Add a preset button to set the glob
		"""
		if label == None:
			label = mask
		
		btn = gtk.Button()
		btn.set_label(label)
		
		btn.connect('clicked',self.preset_press,mask)
		
		#TODO: connect signals.
		
		self.presetbox.pack_start(btn,False,False,5)
		
		
		
		
	def getglob(self):
		"""
		Return the currently chosen glob from the GUI, with both pathname
			and glob, e.g '/path/to/files*,morefiles*'
		"""
		return os.path.join(
				self.textbox.get_text(),
				self.globbox.get_text()
			)
			
	@staticmethod		
	def desensitise(text):
		"""
		turn a glob into a case-insensitive version
		The best way I've found to do this, unfortunately, is with [Aa].
		"""
		ret = ""
		for c in text:
			if c.upper() != c or c.lower() != c:
				#has case
				ret += "[" + c.upper() + c.lower() + "]"
			else:
				ret += c
		return ret
		
	@staticmethod
	def betterglob(spec,case_sensitive=False,allow_dirs=False):
		"""
		An improved glob.glob which supports:
		* multiple globs, e.g "/path/to/*.JPG,*.png"
		* case-insensitivity
		* returning only files, not directories
		
		"""
		spec = os.path.expanduser(spec)
		dn = os.path.dirname(spec)
		if not os.path.exists(dn): return [] #nonexistent path
		fp = os.path.basename(spec)
		
		globs = fp.split(",")
		ret = []
		for filespec in globs:
			if case_sensitive:
				search = filespec
			else:
				search = FileGlobberWidget.desensitise(filespec)
			ret += glob.glob(os.path.join(dn,search))
			
		#print("Betterglob(%s): %s" % (spec,ret))
		
		if not allow_dirs:
			stripped=[]
			for f in ret:
				if not os.path.isdir(f):
					stripped.append(f)
			ret = stripped
		
		return ret		
			
	def getfiles(self):
		"""
		Returns a sorted list of files matching the currently chosen glob
		  You should call this to get a useful value from this widget :)
		"""
		files = FileGlobberWidget.betterglob(self.getglob(),self.case_sensitive.get_active())
		return sorted(files)
		
	def getcount(self,widget=None,whocares=None):
		"""
		Get a count of matching files and update self.count.
		"""
		itms = self.getfiles()
		msg = "%1d File" % len(itms)
		if len(itms) != 1: msg += "s"
		
		if len(itms) > 0:
			self.count.set_sensitive(True)
		else:
			self.count.set_sensitive(False)
			
		self.count.set_label(msg)
		
	def globbox_changed(self,widget = None,something = None):
		self.getcount()
		
		#dlg("Your mum!")
		
	
	def textbox_changed(self,widget = None,something = None):
		self.getcount()
		return FileChooserWidget.textbox_changed(self,widget = None,something = None)

	def choose_file(self,widget = None):
		ret = FileChooserWidget.choose_file(self,widget)
		self.getcount()
		return ret



class MultiLineTextBox(gtk.ScrolledWindow):
	"""
	This is a not-retarded multiline textbox.
		use text() to get / set text without fucking around with buffers
		use buffer to access the textbuffer, view for the textview
		use readonly() to get/set read-only state
	"""
	def __init__(self):
		gtk.ScrolledWindow.__init__(self)

		self.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
		self.buffer = gtk.TextBuffer()
		self.view = gtk.TextView(self.buffer)
		self.view.set_wrap_mode(gtk.WRAP_WORD)
		self.add(self.view)

	def set_font(self,fontname):
		self.buffer.create_tag("font",font=fontname)
		self.buffer.apply_tag_by_name("font",self.buffer.get_start_iter(),self.buffer.get_end_iter())

	def text(self,val = None):
		"""
		Get/set textbox contents
		"""
		if val != None:
			self.buffer.set_text(val)

		return self.buffer.get_text(
					self.buffer.get_start_iter(),
					self.buffer.get_end_iter()
				)

	def append(self,text):
		"""
		append text without replacing the entire contents
		"""
		return self.buffer.insert(self.buffer.get_end_iter(),text)

	def scroll_to_end(self):
		return self.view.scroll_to_iter(self.buffer.get_end_iter(),0)

	def readonly(self,val = None):
		"""
		Make the widget readonly, or find out whether it's readonly
		"""
		if val != None:
			self.view.set_editable(not val)
		return self.view.get_editable()

	def connect(self,signal,callback):
		if signal == "changed":
			#connect to buffer:
			self.buffer.connect(signal,callback)
		else:
			gtk.ScrolledWindow.connect(self,signal,callback)


class ArrayDropdown(gtk.ComboBox):
	"""
	An easy-to-use dropdown list to allow users to choose an item from 
		an array
	"""
	def __init__(self,items = [],selected = 0):
		self.store = gtk.ListStore(str)
		
		gtk.ComboBox.__init__(self,self.store)
		
		cr = gtk.CellRendererText()
		self.pack_start(cr)
		self.add_attribute(cr, 'text', 0)
		self.__items = []
		self.items = items
		self.set_active(selected)
		
	"""
	Items property. Set this to the list you want users to be able to 
	choose from.
	"""
	def setitems(self,val):
		if val == self.__items: 
			#no change
			return False
		self.__items = val
		#store old value
		oldval = self.get_value()
		self.store.clear()
		for val in self.__items:
			self.store.append([val])
		
		if oldval:
			self.set_value(oldval)
		
	def getitems(self):
		return self.__items
	items = property(getitems,setitems)
	
	def get_value(self):
		"""
		Returns the currently selected value
		"""
		iter = self.get_active_iter()
		if not iter: 
			iter = self.store.get_iter_first()
		if not iter: return None
		return self.store.get_value(iter,0)
		
	def set_value(self,value):
		"""
		Set the value
		"""
		if value not in self.items:
			#raise ValueError("Value '%s' not in list" % value)
			#just set the first item as active:
			idx = 0
		else:
			idx = self.items.index(value)
			
		self.set_active(idx)
		
	value = property(get_value,set_value)
	
	


class ArrayScale(gtk.HScale):
	"""
	This is an HScale control (AKA slider) which uses a list of values 
	 that aren't necessarily numeric (or at least linear), but which have an 
	 order. An example it might be a list of values where there are only 
	 certain valid options, like [ 0,1,2,5,10,25,50,75,100 ] or a list of 
	 strings like [ 'none','few', 'some', 'many', 'all' ]. 
	 Other examples: camera f-stop, exposure time, and ISO values.
	 
	 .items is the list of items users can slide through
	 .index is the numeric value of the slider (int, corresponds to an index 
	 		in .items)
	 .on_change allows you to set a callback to run when the value is changed
	 	you can probably use connect('value-changed'), too
	
	"""
	def __init__(self,items = [],selection = 0):
		self.__draw_marks=True
		gtk.HScale.__init__(self)
		self.__items = []
		self.index = selection
		self.items = items
		self.set_update_policy(gtk.UPDATE_CONTINUOUS)
		self.set_digits(0)
		self.__on_change = None
		self.connect('value_changed',self.changed)
		self.connect('format-value',self.format_value)
		self.set_draw_value(True)
		
		
	def format_value(self,widget,value):
		return self.get_value()
		
	def set_draw_value(self,val=True):
		"""
		if this is set to False, don't draw value or marks
		"""
		gtk.HScale.set_draw_value(self,val)
		self.__draw_marks=val
		
	def setitems(self,items):
		#the index of our value may have changed:
		val = self.get_value()
		
		self.__items = items
		
		#reconfigure scale range
		self.set_adjustment(gtk.Adjustment(
			self.index, 0, len(self.__items)-1, 1.0
		))
		self.draw_marks()
		
		if val in self.items:
			self.index = self.items.index(val)
			
	def getitems(self):
		return self.__items
	items = property(getitems,setitems)
	
	def draw_marks(self):
		#add marks on the scale
		self.clear_marks()
		if not self.__draw_marks: return False
		
		max=len(self.__items)-1
		for idx,val in enumerate(self.__items):
			lbl = ""
			if idx == 0 or idx == max or idx == round(max/2):
				lbl = val
			self.add_mark(idx,gtk.POS_BOTTOM,lbl)

	
	def set_value(self,val = None):
		"""
		setter for the currently selected item
		"""
		if val:
			if val in self.items:
				self.index = self.items.index(val)
				gtk.HScale.set_value(self,self.index)
				self.changed()
			else:
				raise ValueError("'%s' is not a valid value for this ArrayScale. Valid values: %s" % (val,self.items))
	
	def next(self,widget=None):
		"""
		Go to the next item
		widget is ignored, used for connecting gtk signals
		"""
		if self.index < (len(self.items)-1):
			#can only go next if not already at end
			self.index+=1
			gtk.HScale.set_value(self,self.index)
			self.changed()
	
	def prev(self,widget=None):
		"""
		Go to the previous item
		widget is ignored, used for connecting gtk signals
		"""
		if self.index >0:
			#can only go next if not already at end
			self.index-=1
			gtk.HScale.set_value(self,self.index)
			self.changed()
	
	
	def get_value(self):
		if len(self.items) == 0:
			#no items
			return None
		if len(self.items) < (self.index - 1):
			#not enough items
			return self.items[0]
			
		return self.items[self.index]
		
	value = property(get_value,set_value)
	
	def on_change(self,callback):
		"""
		Set a callback method to run when the value changes.
		callback method should take one argument, the GtkArrayScale
		"""
		self.__on_change = callback
			
	def changed(self,widget = None):
		"""
		Called when the value is changed
		"""
		rv = gtk.HScale.get_value(self)
		prev = self.index
		if widget:
			self.index = int(round(rv))
			"""
			when a gtk HScale has draw_value set to false, it 
			doesn't round the value and you get a float. We want it
			to clamp to the integers. So we set the value to the 
			integer here to achieve that:
			"""
			#widget.set_value(self.index)
			gtk.HScale.set_value(widget,self.index)
		
		#we need to compare with the previous value because we get many
		# value_changed signals from the scale while sliding,
		# but the value might not have actually changed due to the rounding
		# issue described above
		if (self.index != prev) and self.__on_change:
			self.__on_change(self)



class PropertyEditor(gtk.HBox):
	"""
	This is a wicked-sick pseudo-polymorphic GTK control which is linked
		to an object property.

	object 'property' is a misnomer - this should actually be a getter /
		setter method, jquery-style

	it takes a 'type' parameter which determines the type of control.
		The type is either a class or a string
		supported types:
			int / float - spinbutton
			str			- textbox
			bool		- checkbox
			'label'		- (TODO) label. readonly and str implied.
			'text' 		- string / multi-line
			'array'		- Array of strings in a GtkListView
			'dir'		- Directory
			'file:glob;glob;'
						- File. glob(s) specify filemasks in chooser
			'scale:min:max:places' - int/float slider with min and max
							values. Places is decimal places - 0 implies
							int.

	You can provide a label. If type is a bool, this is probably a good idea.
		If you provide a label for a textbox, a label will be added to the
		left

	Supports readonly properties: If readonly is set, the linked property
		is never called with a value so a parameter is not necessary

	Provides several helper functions to get/set value, enable/disable, etc

	"""
	def __init__(self, prop , controltype = str,
			label = None, readonly = False):

		"""
		Create a Property Editor

		@param prop method			the method to be bound to the control
										value. it should be a getter/setter:
										returning the value, and optionally
										setting it when a value is passed

		@param type class|string	the type of value / control to use.

		@param label string			The label for the control.

		@param readonly bool		Whether the control value can be edited
										If true, the control is disabled.

		"""

		gtk.HBox.__init__(self)

		self.set_homogeneous(False)

		add_label = False
		#if true, a label is added
		label_top = False
		#if false, label is to the left
		label_align = 1
		#halign. 0: left, 1: right

		connect = True
		#connect self.control.signal
		signal = "changed"
		#signal to connect
		pack = True
		#should self.control be packed
		expand = True

		"""
		setup self.control based on controltype:
		"""
		if controltype == bool:
			#check box
			self.control = gtk.CheckButton()
			#self.control.set_size_request(1,20)
			expand = False
			self.control.set_label(label)
			signal = "toggled"

		elif controltype == str:
			#textbox
			self.control = gtk.Entry()
			add_label = True

		elif controltype == int or controltype == float:
			#spinbutton
			self.control = gtk.SpinButton(None,1)
			self.control.set_range(-sys.maxint -1,sys.maxint)
			self.control.set_increments(1,5)
			if controltype == float:
				self.control.set_digits(2)
			signal = "value-changed"
			add_label = True

		elif controltype == "text":
			self.control = MultiLineTextBox()
			add_label = True
			label_top = True
			label_align = 0

		elif controltype == "array":
			self.control = ArrayEditor(label)
			self.control.set_size_request(1,120)
			add_label = False
			label_top = True
			label_align = 0

		elif controltype[0:3] == "dir":
			self.control = FileChooserWidget("dir")
			label_top = True
			label_align = 0
			add_label = True

		elif controltype[0:4] == "file":
			if controltype == "file":
				mask = "*"
			else:
				mask = controltype[5:len(controltype)]
				mask = mask.split(";")

			self.control = FileChooserWidget(mask,prop())
			label_top = True
			label_align = 0
			add_label = True
		elif controltype[0:5] == "scale":
			blah,minimum,maximum,scale = controltype.split(":")
			self.control = gtk.HScale(
				gtk.Adjustment( #note that adjustment initial val is 0, we set it later with refresh()
					0,float(minimum), float(maximum),1, 5 #TODO: scale options for increments.
			))
			self.control.set_digits(int(scale))
			add_label=True
			signal="value-changed"

		else:
			raise Exception("GtkPropertyEditor does not support Type '%s'" % controltype)

		box = self
		#container where self.control goes

		if add_label:
			#add a label to the left:
			if label == None:
				label=""
			l = gtk.Label()
			l.set_markup(label)
			l.set_alignment(label_align,0.5)
			if label_top:
				box = gtk.VBox()
				box.pack_start(l,False,True,5)
				self.pack_start(box,True,True,5)
				#expand should be true in the hbox:
				#expand = True
			else:
				self.pack_start(l,False,True,5)

		if readonly: self.disable()

		if connect:
			self.control.connect(signal,self.update_property)

		if pack:
			box.pack_start(self.control,expand,True,5)

		self._updating = False
		self.property = prop
		self.type = controltype

		self.readonly = readonly

		self.refresh()

	def enabled(self,val = None):
		if val != None:
			self.set_sensitive(bool(val))
		return self.get_sensitive()

	def disable(self):
		return self.enabled(False)
	def enable(self):
		return self.enabled(True)

	def value(self,val = None):
		"""
		Get / Set the value of the control
		"""
		if val != None:
			# setter:

			#print "set value of '%s' to '%s'" % (self.property, val)
			self._updating = True

			if isinstance(self.control,gtk.SpinButton):
				self.control.set_value(val)

			elif isinstance(self.control,gtk.CheckButton):
				self.control.set_active(bool(val))
			elif isinstance(self.control,GtkMultiLineTextBox):
				self.control.text(str(val))

			elif isinstance(self.control,GtkArrayEditor):
				self.control.val(val)

			elif isinstance(self.control,GtkFileChooserWidget):
				self.control.val(val)
			elif isinstance(self.control,gtk.HScale):
				self.control.set_value(float(val))

			elif isinstance(self.control,gtk.Entry):
				#the check against gtk.Entry needs to be last - some items
				#	like SpinButtons are also instances of gtk.entry
				self.control.set_text(val)

			self._updating = False

		#getter:
		val = None

		if isinstance(self.control,gtk.SpinButton):
			if self.type == int:
				val = self.control.get_value_as_int()
			else:
				val = self.control.get_value()
		elif isinstance(self.control,GtkMultiLineTextBox):
			val = self.control.text()
		elif isinstance(self.control,GtkArrayEditor):
			val = self.control.val()
		elif isinstance(self.control,GtkFileChooserWidget):
			val = self.control.val()

		elif isinstance(self.control,gtk.CheckButton):
			val = self.control.get_active()
		elif isinstance(self.control,gtk.HScale):
			val = self.control.get_value()

		elif isinstance(self.control,gtk.Entry):
			#Entry check needs to be last, see above
			val = self.control.get_text()

		#print "value of '%s' is '%s'" % (self.property, val)

		return val

	def update_property(self,widget):
		"""
		Update the linked property. Called when the control changes
		"""

		if self._updating or self.readonly:
			return False

		self.property(self.value())

	def refresh(self):
		"""
		Update the control with the value of the property.
			This should be called if the value changes
		"""
		self.value(self.property())


""" 

Compatibility classes for old GtkThing naming convention:

When we first wrote gui.py, classes were named GtkSomeWidget.
	In 2020, we renamed these, starting with BetterDialog and FileChooserWidget.
	Here, we alias the old names to the new, so that code written 
	for the old classes still works.
	
TODO(?): fire a deprecated warning if these are used?

"""
class GtkIcon(Icon):
	#compatibility class, @see Icon.
	pass

class GtkYesNoDialog(YesNoDialog):
	#compatibility class, @see YesNoDialog.
	pass

class GtkOKDialog(OKDialog):
	#compatibility class, @see OKDialog.
	pass

class GtkTextInputDialog(TextInputDialog):
	#compatibility class, @see TextInputDialog.
	pass

class GtkProgressDialog(ProgressDialog):
	#compatibility class, @see ProgressDialog.
	pass

class GtkColumnBox(ColumnBox):
	#compatibility class, @see ColumnBox.
	pass

class GtkArrayEditor(ArrayEditor):
	#compatibility class, @see ArrayEditor.
	pass

class GtkMarkupButton(MarkupButton):
	#compatibility class, @see MarkupButton.
	pass

class GtkMarkupToggle(MarkupToggle):
	#compatibility class, @see MarkupToggle.
	pass

class GtkLeftAlignLabel(LeftAlignLabel):
	#compatibility class, @see LeftAlignLabel.
	pass

class GtkMultiLineTextBox(MultiLineTextBox):
	#compatibility class, @see MultiLineTextBox.
	pass

class GtkArrayDropdown(ArrayDropdown):
	#compatibility class, @see ArrayDropdown.
	pass

class GtkArrayScale(ArrayScale):
	#compatibility class, @see ArrayScale.
	pass

class GtkPropertyEditor(PropertyEditor):
	#compatibility class, @see PropertyEditor.
	pass

class GtkFileChooserWidget(FileChooserWidget):
	#compatibility class, @see FileChooserWidget.
	pass

class GtkBetterDialog(BetterDialog):
	#compatibility class, @see BetterDialog.
	pass



"""
Test code for GtkPropertyEditor:
"""

#TODO: remove test code
class TestClass:
	def __init__(self):
		self._mert = "MERT!"
		self._foo = True
		self._bork = ['foo',42]

	def mert(self,val = None):
		if val != None:
			self._mert = val
		return self._mert
	def foo(self,val = None):
		if val != None:
			self._foo = val
		return self._foo
	def bork(self,val = None):

		if val != None:
			self._bork = val
		return self._bork

	def column_box_test(self,columns = 4,colspan_even = 2,colspan_odd = 1):
		box = ColumnBox(columns)
		for f in range(1,10):

			if f % 2 == 0:
				colspan = colspan_even
			else:
				colspan = colspan_odd
			lbl = gtk.Label("Item %1d, colspan: %1d" % (f,colspan))
			box.add_item(lbl, colspan)
		box.show()
		return box

def test_propertyeditor():
	d = gtk.Dialog()
	c = d.get_content_area()

	t = TestClass()

	e = PropertyEditor(t.mert,"dir","Mert:",False)
	#e.enabled(False)
	f = PropertyEditor(t.foo,"scale:0:100:3","Foo:")
	g = PropertyEditor(t.bork, "array" ,"Bork:",False)

	c.pack_start(e)
	c.pack_start(f)
	c.pack_start(g)

	c.pack_start(t.column_box_test(),False,True)

	d.show_all()
	d.run()
	d.destroy()

	print("mert: '%s'" % t.mert())
	print("foo: %s" % t.foo())
	a = t.bork()
	print("bork: %s (%s)" % (a,a.__class__))

	exit(42)

if __name__ == '__main__':
	OKDialog.show(None,"TODO: GUI library demo!")

#test_propertyeditor()


